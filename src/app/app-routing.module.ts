import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormPlayerComponent } from './components/form-player/form-player.component';
import { ListplayersComponent } from './components/listplayers/listplayers.component';


const routes: Routes = [
  { path: "", component: ListplayersComponent },
  { path: "form-player/:param", component: FormPlayerComponent },
  { path: "form-player/:param", component: FormPlayerComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
