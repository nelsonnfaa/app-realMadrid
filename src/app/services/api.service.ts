import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IPlayer, IPlayerlist } from '../interfaces/interface.players';

const baseUrl = 'http://localhost:8000/api/';
const playerUrl = `${baseUrl}realmadrid/`;

@Injectable({
  providedIn: "root"
})
export class ApiService {
  playersListApi: IPlayerlist[];
  playerToModify: IPlayer;
  constructor(private httpClient: HttpClient) {
    this.playerToModify = Object();
  }

  public fetchPlayers() {
    console.log(playerUrl);
    return this.httpClient.get<IPlayerlist[]>(playerUrl);
  }
  public getPlayers() {
    this.fetchPlayers();
    console.log("jugadores");
    console.log(this.playersListApi);
    return this.playersListApi;
  }
 
  public findPlayer(id: string) {
      //this.findFechPlayer(id);
      return this.playerToModify;
  } 
  public findFechPlayer(id: string) {
    //return this.playersListApi.find(element => element.id === id);
    return this.httpClient.get<IPlayer>(playerUrl + id)
  }
  public deletePlayerId(id: number): void {}
}
