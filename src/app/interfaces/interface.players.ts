export interface IPlayer {
  id: string;
  num: string;
  name: string;
  position: string;
  image: string;
}
export interface IPlayerlist
{
    id: string,
    name: string,
    image: string,
}