import { Injectable } from "@angular/core";
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { IPlayer, IPlayerlist } from 'src/app/interfaces/interface.players';

@Component({
  selector: "app-listplayers",
  templateUrl: "./listplayers.component.html",
  styleUrls: ["./listplayers.component.scss"]
})
export class ListplayersComponent implements OnInit {
  playersList: IPlayerlist[];
  constructor(private apiService: ApiService) {
    this.playersList = [];
  }
  
  ngOnInit() {
    this.apiService.fetchPlayers().subscribe(data => {
      this.playersList = data;
      console.log(this.playersList);
    });
  }
  public deletePlayer(id: number): void {
    this.apiService.deletePlayerId(id);
  }
}
