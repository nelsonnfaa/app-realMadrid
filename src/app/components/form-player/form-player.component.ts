import { Component, OnInit } from "@angular/core";
import { ApiService } from "src/app/services/api.service";
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, FormBuilder } from "@angular/forms";
import { IPlayer } from "src/app/interfaces/interface.players";

@Component({
  selector: "app-form-player",
  templateUrl: "./form-player.component.html",
  styleUrls: ["./form-player.component.scss"]
})
export class FormPlayerComponent implements OnInit {
  playerValues: FormGroup;
  action: number | string;
  playerToModify: IPlayer;
  titulo:string = "Nuevo Juagador"
  id_player: string;
  image_player: string;
  name_player: string;
  position_player: string;
  num_player: string;
  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: ApiService,
    private formBuilder: FormBuilder /*, private router: Router*/
  ) {
    this.playerToModify = Object();
    this.action = this.activatedRoute.snapshot.params.param;
    if (this.action > 0) {
      const idplayer = this.action.toString();
      this.apiService.findFechPlayer(idplayer).subscribe((data: IPlayer) => {
        this.playerToModify = data;
        console.log("*****jugador****")
        console.log(this.playerToModify);
      })
      this.id_player = this.playerToModify.id;
      console.log("*****jugador id****");
      console.log(this.id_player);
      this.image_player = this.playerToModify.image;
      this.name_player = this.playerToModify.name;
      this.position_player = this.playerToModify.position;
      this.num_player = this.playerToModify.num;
      this.titulo = "Modificar Jugador"
      console.log("*****jugador colocado****");
      console.log(this.playerToModify);
    }
  }

  ngOnInit() {
    
    this.playerValues = this.formBuilder.group({
      id: this.id_player,
      image: this.image_player,
      name: this.name_player,
      position: this.position_player,
      num: this.num_player
    });
  }
}
