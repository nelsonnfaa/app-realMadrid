<?php


namespace App\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
function conexion()
    {
        $dsn = "mysql:host=localhost;dbname=realmadrid;port=3306;charset=utf8mb4";
        try {
            $pdo = new \PDO($dsn, 'root', '');
        } catch (\Exception $e) {
            die("ERROR AL CONECTARSE A LA BASE DE DATOS");
        }
        return $pdo;
    }
class FixturesPlayers extends Command
{
    protected static $defaultName = 'app:players-fixtures';

    public function configure()
    {
        $this->setDescription("Genero las fixtures de players del Real Madrid");
    }
    
    /*public function conexion()
    {
        $dsn = "mysql:host=mysql;dbname=demo;port=3306;charset=utf8mb4";
        try {
            $pdo = new \PDO($dsn, 'user', 'pass');
        } catch (\Exception $e) {
            die("ERROR AL CONECTARSE A LA BASE DE DATOS");
        }
        return $pdo;
    }*/
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $pdo = conexion();

        if ($pdo->exec('DROP TABLE IF EXISTS Jugadores') === false) {
            die("ERROR AL VACIAR LA TABLA Jugadores");
        }

        if ($pdo->exec('CREATE TABLE IF NOT EXISTS realmadridplayers (id_players INT NOT NULL AUTO_INCREMENT, name_players VARCHAR(100), position_players VARCHAR(80), num_players VARCHAR(80), image_players VARCHAR(80), PRIMARY KEY (id_players))') === false) {
            die("ERROR AL CREAR LA TABLA Jugadores: " . $pdo->errorInfo());
        } else {
            $output->writeln("CREADA LA TABLA Jugadores\n");
        }

        $output->writeln("INICIALIZANDO TRANSACCIÓN PARA IMPORTAR DATOS... \n");

        $datos = file_get_contents("./fixtures/dataPlayers");
        $players = json_decode($datos, true);

        try {
            $pdo->beginTransaction();
            
            $stmt = $pdo->prepare('INSERT INTO realmadridplayers (name_players, position_players, num_players, image_players) VALUES (:name_players, :position_players, :num_players, :image_players)');
            $stmt->bindParam(':name_players', $name_players);
            $stmt->bindParam(':position_players', $position_players);
            $stmt->bindParam(':num_players', $num_players);
            $stmt->bindParam(':image_players', $image_players);

            foreach ($players['players'] as $players){

                $name_players = $players['name'];
                $position_players = $players['position'];
                $num_players = $players['num'];
                $image_players = $players['src'];
                if ($name_players) {
                    $stmt->execute();
                }
                $output->writeln( "ha insertado $name_players");
            }
            $pdo->commit();
        } catch (\Exception $e) {
            $output->writeln( "ha habido un error ".$e->getMessage());
            $pdo->rollback();
        }
        $output->writeln("Proceso finalizado");
        return 1;
    }
}