<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

function conexion()
{
    $dsn = "mysql:host=localhost;dbname=realmadrid;port=3306;charset=utf8mb4";
    try {
        $pdo = new \PDO($dsn, 'root', '');
    } catch (\Exception $e) {
        die("ERROR AL CONECTARSE A LA BASE DE DATOS");
    }
    return $pdo;
}
/*function conexion()
{
$dsn = "mysql:host=mysql;dbname=demo;port=3306;charset=utf8mb4";
try {
$pdo = new \PDO($dsn, 'user', 'pass');
} catch (\Exception $e) {
die("ERROR AL CONECTARSE A LA BASE DE DATOS");
}
return $pdo;
}*/

class ApiControllerPlayers extends AbstractController
{
    /**
     * @Route("/api/realmadrid", methods={"GET"})
     */
    public function getRealMadrid()
    {
        $pdo = conexion();
        $players = $pdo->prepare("Select name_players, image_players from realmadridplayers");
        $players->execute();
        return new JsonResponse($players->fetchAll(\PDO::FETCH_ASSOC));
    }

    /**
     * @Route("/api/realmadrid/{id}", methods={"GET"})
     */
    public function getRealMadridPlayer($id)
    {
        $pdo = conexion();


        foreach ($pdo->query("Select * from realmadridplayers where id_players=$id") as $row) {
            $player['id'] = $row['id_players'];
            $player['name_players'] = $row['name_players'];
            $player['position_players'] = $row['position_players'];
            $player['num_players'] = $row['num_players'];
            $player['image_players'] = $row['image_players'];
        }
        return new JsonResponse($player);
    }

    /**
     * @Route("/api/realmadrid", methods={"POST"})
     */
    public function insertPlayer(Request $request)
    {
        $player = $request->get('player');

        $playerDecode = json_decode($player, true);

        $pdo = conexion();

        try {
            $pdo->beginTransaction();

            $stmt = $pdo->prepare('INSERT INTO pelicula (titulo, descripcion) VALUES (:titulo, :descripcion)');
            $stmt->bindParam(':titulo', $titulo);
            $stmt->bindParam(':descripcion', $descripcion);

            $titulo = $peliculaDecode['titulo'];
            $descripcion = $peliculaDecode['descripcion'];
            if ($titulo) {
                $stmt->execute();
            }

            $pdo->commit();
        } catch (\Exception $e) {
            $pdo->rollback();
        }

        return new JsonResponse($peliculaDecode);
    }

    /**
     * @Route("/api/pelicula/{id}", methods={"PUT"})
     */
    public function updatePelicula(Request $request, $id)
    {
        $pelicula = $request->get('pelicula');

        $peliculaDecode = json_decode($pelicula, true);

        $pdo = conexion();

        try {
            $pdo->beginTransaction();
            $stmt = $pdo->prepare("UPDATE pelicula SET titulo=:titulo, descripcion=:descripcion WHERE id=$id");
            $stmt->bindParam(':titulo', $titulo);
            $stmt->bindParam(':descripcion', $descripcion);

            $titulo = $peliculaDecode['titulo'];
            $descripcion = $peliculaDecode['descripcion'];
            if ($titulo) {
                $stmt->execute();
            }

            $pdo->commit();
        } catch (\Exception $e) {
            echo $e->getMessage();
            $pdo->rollback();
        }

        return new JsonResponse($peliculaDecode);
    }
    /**
     * @Route("/api/pelicula/{id}", methods={"DELETE"})
     */
    public function deletePelicula($id)
    {
        $pdo = conexion();

        try {
            //$pdo->beginTransaction();
            $result = $pdo->query("DELETE FROM pelicula WHERE id=$id");

            //$pdo->commit();
        } catch (\Exception $e) {
            echo $e->getMessage();
            $pdo->rollback();
        }

        return new JsonResponse($result);
    }
}
